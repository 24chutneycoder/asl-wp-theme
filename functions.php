<?php

add_action('after_setup_theme','bones_ahoy', 15);
function bones_ahoy() {

    // launching operation cleanup
    add_action('init', 'bones_head_cleanup');
    // remove WP version from RSS
    add_filter('the_generator', 'bones_rss_version');
    // remove pesky injected css for recent comments widget
    add_filter( 'wp_head', 'bones_remove_wp_widget_recent_comments_style', 1 );
    // clean up comment styles in the head
    add_action('wp_head', 'bones_remove_recent_comments_style', 1);
    // clean up gallery output in wp
    add_filter('gallery_style', 'bones_gallery_style');

    // enqueue base scripts and styles
    add_action('wp_enqueue_scripts', 'bones_scripts_and_styles', 999);
    // ie conditional wrapper
    add_filter( 'style_loader_tag', 'bones_ie_conditional', 10, 2 );

    // launching this stuff after theme setup
    //add_action('after_setup_theme', 'bones_theme_support');
    // adding sidebars to Wordpress (these are created in functions.php)
    add_action( 'widgets_init', 'bones_register_sidebars' );
    // adding the bones search form (created in functions.php)
    add_filter( 'get_search_form', 'bones_wpsearch' );

    // cleaning up random code around images
    add_filter('the_content', 'bones_filter_ptags_on_images');
    // cleaning up excerpt
    add_filter('excerpt_more', 'bones_excerpt_more');

} /* end bones ahoy */

/**
 * WP Head Cleanup
 */

function bones_head_cleanup() {
    // category feeds
    // remove_action( 'wp_head', 'feed_links_extra', 3 );
    // post and comment feeds
    // remove_action( 'wp_head', 'feed_links', 2 );
    // EditURI link
    remove_action( 'wp_head', 'rsd_link' );
    // windows live writer
    remove_action( 'wp_head', 'wlwmanifest_link' );
    // index link
    remove_action( 'wp_head', 'index_rel_link' );
    // previous link
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
    // start link
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
    // links for adjacent posts
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
    // WP version
    remove_action( 'wp_head', 'wp_generator' );

} /* end bones head cleanup */

// remove WP version from RSS
function bones_rss_version() { return ''; }

// remove injected CSS for recent comments widget
function bones_remove_wp_widget_recent_comments_style() {
   if ( has_filter('wp_head', 'wp_widget_recent_comments_style') ) {
      remove_filter('wp_head', 'wp_widget_recent_comments_style' );
   }
}

// remove injected CSS from recent comments widget
function bones_remove_recent_comments_style() {
  global $wp_widget_factory;
  if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
    remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
  }
}

// remove injected CSS from gallery
function bones_gallery_style($css) {
  return preg_replace("!<style type='text/css'>(.*?)</style>!s", '', $css);
}

/**
 * Scripts and Styles
 */

function bones_scripts_and_styles() {

  if (!is_admin()) {

    // Deregister Things
    add_action( 'wp_print_scripts', 'sherman_deregister_scripts', 100 );
    function sherman_deregister_scripts() {

        wp_deregister_script( 'jquery' );
    }

    wp_register_style( 'legacy-css', '//sherman.library.nova.edu/cdn/styles/css/public-global/public-1.9.9.min.css', array(), '', 'all' );
    wp_register_style( 'public-global', '//sherman.library.nova.edu/cdn/styles/css/public-global/public-2.1.min.css', array(), '', 'all' );
    wp_register_style( 'critical-css', '//sherman.library.nova.edu/cdn/styles/css/public-global/critical.css', array(), '', 'all' );

    if ( !is_main_site() && !is_front_page() ) {

        if ( !is_post_type_archive( 'spotlight_events' ) ) :
            wp_register_script( 'pls-js', '//sherman.library.nova.edu/cdn/scripts/min/scripts.min.js', array(), '', true );
        endif;

    }

    global $blog_id;
    if ( $blog_id == 25 || $blog_id == 26 ) :
        wp_enqueue_style( 'legacy-css' );
    else :
        wp_enqueue_style( 'critical-css' );
    endif;

    /*if ( !is_main_site() ) {

        if ( !is_post_type_archive( 'list' ) || !is_post_type_archive('spotlight_events') ) :
            wp_enqueue_script( 'pls-js' );
        endif;

    }*/


  }
}


// adding the conditional wrapper around ie stylesheet
// source: http://code.garyjones.co.uk/ie-conditional-style-sheets-wordpress/
function bones_ie_conditional( $tag, $handle ) {
    if ( 'pls-ie-only' == $handle )
        $tag = '<!--[if lt IE 9]>' . "\n" . $tag . '<![endif]-->' . "\n";
    return $tag;
} // MS Note: This was lte IE 9. I changed this because it appears IE9 is now up to snuff.

/**
 * Related Posts
 */
// Related Posts Function (call using bones_related_posts(); )
function bones_related_posts() {
    echo '<ul id="bones-related-posts">';
    global $post;
    $tags = wp_get_post_tags($post->ID);
    if($tags) {
        foreach($tags as $tag) { $tag_arr .= $tag->slug . ','; }
        $args = array(
            'tag' => $tag_arr,
            'numberposts' => 5, /* you can change this to show more */
            'post__not_in' => array($post->ID)
        );
        $related_posts = get_posts($args);
        if($related_posts) {
            foreach ($related_posts as $post) : setup_postdata($post); ?>
                <li class="related_post"><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
            <?php endforeach; }
        else { ?>
            <?php echo '<li class="no_related_post">No Related Posts Yet!</li>'; ?>
        <?php }
    }
    wp_reset_query();
    echo '</ul>';
} /* end bones related posts function */

/**
 * Page Navigation
 */

// Numeric Page Navi (built into the theme by default)
function bones_page_navi($before = '', $after = '') {
    global $wpdb, $wp_query;
    $request = $wp_query->request;
    $posts_per_page = intval(get_query_var('posts_per_page'));
    $paged = intval(get_query_var('paged'));
    $numposts = $wp_query->found_posts;
    $max_page = $wp_query->max_num_pages;
    if ( $numposts <= $posts_per_page ) { return; }
    if(empty($paged) || $paged == 0) {
        $paged = 1;
    }
    $pages_to_show = 7;
    $pages_to_show_minus_1 = $pages_to_show-1;
    $half_page_start = floor($pages_to_show_minus_1/2);
    $half_page_end = ceil($pages_to_show_minus_1/2);
    $start_page = $paged - $half_page_start;
    if($start_page <= 0) {
        $start_page = 1;
    }
    $end_page = $paged + $half_page_end;
    if(($end_page - $start_page) != $pages_to_show_minus_1) {
        $end_page = $start_page + $pages_to_show_minus_1;
    }
    if($end_page > $max_page) {
        $start_page = $max_page - $pages_to_show_minus_1;
        $end_page = $max_page;
    }
    if($start_page <= 0) {
        $start_page = 1;
    }
    echo $before.'<div class="hero--small align-center"><nav class="pagination" role="navigation">'."";
    if ($start_page >= 2 && $pages_to_show < $max_page) {
        $first_page_text = "First";
        echo '<a class="pagination__link" href="'.get_pagenum_link().'" title="'.$first_page_text.'">'.$first_page_text.'</a>';
    }

    //echo '<span class="pagination__link">';
    //previous_posts_link('«');
    //echo '</span>';

    for($i = $start_page; $i  <= $end_page; $i++) {
        if($i == $paged) {
            echo '<a class="pagination__link is-active">'.$i.'</a>';
        } else {
            echo '<a class="pagination__link" href="'.get_pagenum_link($i).'">'.$i.'</a>';
        }
    }
    //echo '<span class="pagination__link">';
    //next_posts_link('»');
    //echo '</span>';
    if ($end_page < $max_page) {
        $last_page_text = "Last";
        echo '<li class="bpn-last-page-link"><a href="'.get_pagenum_link($max_page).'" title="'.$last_page_text.'">'.$last_page_text.'</a></li>';
    }
    echo '</nav></div>'.$after."";
} /* end page navi */

/*********************
RANDOM CLEANUP ITEMS
*********************/

// remove the p from around imgs (http://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/)
function bones_filter_ptags_on_images($content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

// This removes the annoying […] to a Read More link
function bones_excerpt_more($more) {
    global $post;
    // edit here if you like
    return '...  <a href="'. get_permalink($post->ID) . '" title="Read '.get_the_title($post->ID).'">Read more &raquo;</a>';
}


require_once('library/admin.php');

add_filter( 'img_caption_shortcode', 'cleaner_caption', 10, 3 );
function cleaner_caption( $output, $attr, $content ) {

    /* We're not worried about captions in feeds, so just return the output here. */
    if ( is_feed() )
        return $output;

    /* Set up the default arguments. */
    $defaults = array(
        'id' => '',
        'align' => 'alignnone',
        'width' => '',
        'caption' => ''
    );

    /* Merge the defaults with user input. */
    $attr = shortcode_atts( $defaults, $attr );

    /* If the width is less than 1 or there is no caption, return the content wrapped between the [caption]< tags. */
    if ( 1 > $attr['width'] || empty( $attr['caption'] ) )
        return $content;

    /* Set up the attributes for the caption <div>. */
    $attributes = ( !empty( $attr['id'] ) ? ' id="' . esc_attr( $attr['id'] ) . '"' : '' );
    $attributes .= ' class="wp-caption ' . esc_attr( $attr['align'] ) . '"';
    $attributes .= ' style="width: ' . esc_attr( $attr['width'] ) . 'px"';

    /* Open the caption <figure>. */
    $output = '<figure' . $attributes .'>';

    /* Allow shortcodes for the content the caption was created for. */
    $output .= do_shortcode( $content );

    /* Append the caption text. */
    $output .= '<figcaption class="wp-caption-text">' . $attr['caption'] . '</figcaption>';

    /* Close the caption </div>. */
    $output .= '</figure>';

    /* Return the formatted, clean caption. */
    return $output;
}

/* ==================
 * Theme Support
 * ================== */
add_theme_support('post-thumbnails');
add_theme_support( 'custom-background',
    array(
    'default-image' => '',  // background image default
    'default-color' => '', // background color default (dont add the #)
    'wp-head-callback' => '_custom_background_cb',
    'admin-head-callback' => '',
    'admin-preview-callback' => ''
    )
);
add_theme_support('automatic-feed-links');
add_theme_support( 'post-formats',
    array(
        'aside',             // title less blurb
        'gallery',           // gallery of images
        'link',              // quick link to other site
        'image',             // an image
        'quote',             // a quick quote
        'status',            // a Facebook like status update
        'video',             // video
        'audio',             // audio
        'chat'               // chat transcript
    )
);
add_theme_support( 'menus' );

/* ==================
 * $SIDEBARS
 * ================== */
// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
    register_sidebar(array(
        'id' => 'home',
        'name' => 'Front Page Sidebar',
        'description' => 'This sidebar appears specifically on the front page. Use wisely. This sidebar has no container.',
        'before_widget' => '<div id="%1$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="delta no-margin section-title">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'id' => 'single',
        'name' => 'Post Sidebar',
        'description' => 'This sidebar appears on posts. Use wisely. This sidebar has no container.',
        'before_widget' => '<div id="%1$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="delta no-margin section-title">',
        'after_title' => '</h3>',
    ));

} // don't remove this bracket!

/* ====================== *
 * $Short Codes
 * ====================== */
// [note count="1" note="See such and such."]
// Footnotes are populated with javascript after
// page load, so if the body class is .no-js, no notes)
// TO-DO: Do this with php.
function sherman_footnotes( $atts ) {

    extract( shortcode_atts( array(
        'count' => '1',
        'comment' => 'A footnote should be here.'
    ), $atts ) );

    return "<sup><a href='#fn-item$count' id='fn-return$count' data-note='$comment'>$count</a></sup>";
}
add_shortcode( 'note', 'sherman_footnotes' );

// [section id="Something"]
function sherman_section_heading( $atts, $heading = null ) {

    extract( shortcode_atts( array(
            'id' => '1'
        ), $atts ) );

    return "<h3 id='$id'>$heading</h3>";
}
add_shortcode( 'section', 'sherman_section_heading' );

function sherman_event_feed_embed( $atts ) {

    extract( shortcode_atts( array(
        'audience' => 'public',
        'num'   => '3',
        'series' => ''
    ), $atts ) );

    $content = '<div ng-controller="EventController as ec" data-audience="' . $audience . '" data-series="' . $series . '">
    <article data-ng-repeat="event in ec.events" class="card clearfix">
      <span class="card__color-code ' . ( $audience === 'kids' ? 'card__color-code--kids' : ( $audience === 'teens' ? 'card__color-code--teens' : '' ) ) . '"></span>
      <header class="card__header" style="margin-bottom: .5em;">
        <a ng-href="{{ event.url }}" class="link link--undecorated _link-blue">
          <h3 class="menu__item__title" style="margin-bottom: .25em;">{{ event.title }}</h3>
        </a>
        <p class="no-margin small-text" >
          <time class="time">
            <span itemprop="startDate"><b>{{ event.start }}</b></span>
            <span class="time__hours" style="color: #999;">{{ event.from }} - {{ event.until }}</span>
          </time>
          <span ng-bind-html="event.series"></span>
        </p>
      </header>

      <section class="content">
        <p class="no-margin">{{ event.excerpt }}</p>
      </section>

    </article>
    </div>
    ';

    return $content;
}
add_shortcode( 'event', 'sherman_event_feed_embed' );

/* ==================
 * Comments
 * ================== */
function bones_comments($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
    <article id="comment-<?php comment_ID(); ?>" class="<?php comment_class(); ?> clearfix">
        <header class="comment-author vcard">
            <?php /*
                this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
                echo get_avatar($comment,$size='32',$default='<path_to_url>' );
            */ ?>
            <!-- custom gravatar call -->
            <!-- end custom gravatar call -->
            <?php printf(__('<cite class="fn gamma">%s</cite> <br>'), get_comment_author_link()) ?>
            <time class="delta" datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time('F jS, Y'); ?> </a></time>

        </header>
        <?php if ($comment->comment_approved == '0') : ?>
            <div class="alert info">
                <p><?php _e('Your comment is awaiting moderation.', 'bonestheme') ?></p>
            </div>
        <?php endif; ?>
        <section class="comment_content clearfix">
            <?php comment_text() ?>
        </section>
        <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
        <?php edit_comment_link(__('Edit', 'bonestheme'),'  ','') ?>
    </article>
<?php
} // don't remove this bracket!

/* ==================
 * Search
 * ================== */
// Search Form
function sherman_wpsearch() {
    $form =
        '<form class="has-background no-margin form" role="search" method="get" id="searchform" action="http://sharksearch.nova.edu/search">
            <ul class="no-margin">
                <li class="form__field no-margin">
                    <label class="hide-accessible label" for="searchbox">Search the Library Website</label>
                    <svg class="svg svg--search" style="margin-right: 1em;" viewBox="0 0 32 32"><use xlink:href="#icon-search"></use></svg>
                    <input class="input form__input form__input--full-width search__search-field search__search-field--transparent input--large input--epsilon" type="search" value="' . get_search_query() . '" name="q" id="searchbox" placeholder="'.esc_attr__('Search the Library Website','bonestheme').'" speech />
                </li>
            </ul>


        <input type="hidden" name="client" value="aslritc">
        <input type="hidden" name="proxystylesheet" value="main">
        <input type="hidden" name="output" value="xml_no_dtd">
        <input type="hidden" name="access" value="p">
        <input type="hidden" name="ie" value="UTF-8">
        <input type="hidden" name="oe" value="UTF-8">
        <input type="hidden" name="site" value="aslritc" id="sopswap">
    </form>';
    return $form;
} // don't remove this bracket!


    /*
     * 1. Create your own custom action hook named 'the_action_hook'
     *    with just a line of code. Yes, it's that simple.
     *
     *    The first argument to add_action() is your action hook name
     *    and the second argument is the name of the function that actually gets
     *    executed (that's 'callback function' in geek).
     *
     *    In this case you create an action hook named 'the_action_hook'
     *    and the callback function is 'the_action_callback'.
     */

        add_action( 'asl_wp_theme_site_title_hook', 'asl_wp_theme_site_title_callback', 10, 0 );
        function asl_wp_theme_site_title_callback() {}

    /*
     * 3. When you call do_action() with your action hook name
     *    as the argument, all functions hooked to it with add_action()
     *    (see step 1. above) get are executed - in this case there is
     *    only one, the_action_callback(), but you can attach as many functions
     *    to your hook as you like.
     *
     *    In this step we wrap our do_action() in yet another
     *    function, the_action(). You can actually skip this step and just
     *    call do_action() from your code.
     */
        function asl_wp_theme_site_title() {
            do_action( 'asl_wp_theme_site_title_hook' );
        }


if ( ! function_exists( 'return_asl_wp_theme_site_title' ) ) :
    function return_asl_wp_theme_site_title() {

        echo ( is_main_site( 1 ) ? get_the_title() : '<a class="link link--undecorated" href="'. home_url() .'">' . get_bloginfo( 'name' ) . '</a>' );

    }
    add_filter( 'asl_wp_theme_site_title_hook', 'return_asl_wp_theme_site_title', 11, 0 );
endif;

/**
 * Add excerpt to the page post type
 */
add_action( 'init', 'asl_add_excerpt_to_page' );
function asl_add_excerpt_to_page() {
    add_post_type_support( 'page', 'excerpt' );
}

register_taxonomy( 'library-audience',
    array('spotlight_databases', 'spotlight_events', 'spotlight_reviews', 'spotlight_items', 'post', 'list'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    array('hierarchical' => true,     /* if this is true it acts like categories */
        'labels' => array(
            'name' => __( 'Library Audience', 'bonestheme' ), /* name of the custom taxonomy */
            'singular_name' => __( 'Audience Type', 'bonestheme' ), /* single taxonomy name */
            'search_items' =>  __( 'Search Audiences', 'bonestheme' ), /* search title for taxomony */
            'all_items' => __( 'All Audiences', 'bonestheme' ), /* all title for taxonomies */
            'parent_item' => __( 'Patron Type', 'bonestheme' ), /* parent title for taxonomy */
            'parent_item_colon' => __( 'Patron Type:', 'bonestheme' ), /* parent taxonomy title */
            'edit_item' => __( 'Edit Audience', 'bonestheme' ), /* edit custom taxonomy title */
            'update_item' => __( 'Update Audience', 'bonestheme' ), /* update title for taxonomy */
            'add_new_item' => __( 'Add New Audience', 'bonestheme' ), /* add new title for taxonomy */
            'new_item_name' => __( 'New Audience Name', 'bonestheme' ) /* name title for taxonomy */
        ),
        'show_ui' => true,
        'show_in_rest' => true,
        'query_var' => true,
        'capabilities' => array(
            'manage_terms' => 'admin',
            'edit_terms' => 'admin',
            'delete_terms' => 'admin',
            'assign_terms' => 'edit_posts'
        ),
    )
);

/**
 * Menu Registration
 */
add_action( 'init', 'asl_register_menu_locations' );
function asl_register_menu_locations() {

    register_nav_menus(
        array(
            'main-menu' => __( 'Top Menu' ),
            'context-menu' => __( 'Context Menu' )
        )
    );

}

function wp_multisite_nav_menu( $args = array(), $origin_id = 1 ) {

    global $blog_id;
    $origin_id = absint( $origin_id );

    if ( !is_multisite() || $origin_id == $blog_id ) {
        wp_nav_menu( $args );
        return;
    }

    switch_to_blog( $origin_id );
    wp_nav_menu( $args );
    restore_current_blog();

}

/**
 * Lists Short Code
 */

function sherman_item_list_container_shortcode( $atts, $content = null ) {

    return '<ul class="list list--alternate"> ' . do_shortcode( $content ) . '</ul>';

} add_shortcode( 'item_list', 'sherman_item_list_container_shortcode');


function sherman_item_list_majax_shortcode( $atts ) {

    $a = shortcode_atts( array(
            'author'    => '',
            'bibrecord' => '',
            'isbn'      => '',
            'oclc'      => '',
            'summary'   => '',
            'title'     => '',
            'audio'     => '',
            'ebook'     => '',
            'upc'     => ''
        ), $atts );

    $slug = ( $a['upc'] ? 'upc' : 'isbn' );

    $list_item =
    '<li class="clearfix">

        <div class="col-md--threecol media">
            <a class="record-link" href="//novacat.nova.edu/record=' . esc_attr($a['bibrecord']) . '" onClick="ga( \'send\', \'event\', \'Catalog Items\', \'Click - Cover\', \'' . esc_attr($a['title']) . '\' );">
                <img src="//www.syndetics.com/index.php?' . $slug . '=' . ( $slug === 'upc' ? esc_attr($a['upc']) : trim(esc_attr($a['isbn']))  ) . '/lc.gif&client=novaseu"
                     srcset="//www.syndetics.com/index.php?' . $slug . '=' . ( $slug === 'upc' ? esc_attr($a['upc']) : trim(esc_attr($a['isbn']))  ) . '/lc.gif&client=novaseu 1024w,
                             //www.syndetics.com/index.php?' . $slug . '=' . ( $slug === 'upc' ? esc_attr($a['upc']) : trim(esc_attr($a['isbn']))  ) . '/mc.gif&client=novaseu 720w,
                             //www.syndetics.com/index.php?' . $slug . '=' . ( $slug === 'upc' ? esc_attr($a['upc']) : trim(esc_attr($a['isbn']))  ) . '/sc.gif&client=novaseu 320w"
                     alt="" />
            </a>
        </div>
          <div class="col-md--ninecol">
            <a class="link link--undecorated card__title delta no-margin" href="//novacat.nova.edu/record=' . esc_attr($a['bibrecord']) . '" onClick="ga( \'send\', \'event\', \'Catalog Items\', \'Click - Title\', \'' . esc_attr($a['title']) . '\' );">' . esc_attr($a['title']) . '</a> <br>
            <span class="zeta"> <a class="link link--undecorated" href="//novacat.nova.edu/search/X?SEARCH=a:(' . esc_attr($a['author']) . ')&amp;searchscope=13"> ' . esc_attr($a['author']) . '</a></span>
            <p class="zeta">' . wp_kses_post($a['summary']) . '</p>
        </div>

    </li>';

    return $list_item;

    //return '<span class="' . esc_attr($a['class']) . '">' . $content . '</span>';

} add_shortcode( 'item', 'sherman_item_list_majax_shortcode');

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

/*------------------------------------*\
    #Enable PHP Sessions
\*------------------------------------*/
/**
 * WordPress is stateless, and doesn't allow the use of sessions except
 * for the cookie used to keep users logged in.
 * See: http://silvermapleweb.com/using-the-php-session-in-wordpress/
 */
add_action( 'init', 'asl_wp_theme_start_session', 1 );
function asl_wp_theme_start_session() {
    if ( !session_id() ) {
        session_start();
    }
}
