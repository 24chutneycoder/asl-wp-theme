<?php get_header(); ?>

<div id="content">
  <main id="main" role="main">
      <h1 class="hide-accessible"><?php the_title(); ?></h1>

      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

        <?php if ( has_excerpt() ) : ?>
        <div class="has-background background-base hero">
          <div class="cleafix wrap">
            <p class="col-md--eightcol col--centered"><?php echo get_the_excerpt(); ?></p>
          </div>
        </div>
        <?php endif;?>

  	    <div class="clearfix wrap hero--small">
          <section class="col-md--eightcol col--centered" itemprop="articleBody">
  		      <?php the_content(); ?>
          </section>
  		  </div> <!-- end article section -->

  	    <footer class="article-footer wrap clearfix">

  		    <?php the_tags('<p class="tags"><span class="tags-title">Tags:</span> ', ', ', '</p>'); ?>

  	    </footer> <!-- end article footer -->

      </article> <!-- end article -->

      <?php endwhile; ?>

      <?php else : ?>

  	    <article id="post-not-found" class="hentry clearfix">
  	    	<header class="article-header">
  	    		<h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
  	    	</header>
  	    	<section class="post-content">
  	    		<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
  	    	</section>
  	    	<footer class="article-footer">
  	    	    <p><?php _e("This is the error message in the page.php template.", "bonestheme"); ?></p>
  	    	</footer>
  	    </article>

      <?php endif; ?>

  </main> <!-- end #main -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
