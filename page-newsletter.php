<?php get_header(); ?>

		<div id="content">

				    <div id="main" role="main">

				    	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="has-background background-base hero">

									<div class="clearfix wrap">
									<h1 class="hide-accessible" itemprop="headline">
										<?php the_title(); ?>
									</h1>

									<div class="col-md--eightcol col--centered">
										<p>
											<?php echo get_the_excerpt(); ?>
										</p>

										<form class="form" action="http://systems.library.nova.edu/form/view.php?id=26" method="post" role="form">
											<ul>
												<li class="form__field">
													<label class="form__label hide-accessible" for="element_2">
														Email Address
													</label>
													<input style="border-bottom: 1px solid white; border-radius: 0;" id="element_2" class="form__input input--transparent form__input--full-width" type="email" name="element_2" placeholder="your@email.com"/>
												</li>

												<li class="form__field">
													<input class="button button--primary--alt zeta" id="saveForm" type="submit" value="Sign Up" name="submit" />
												</li>

												<input type="hidden" value="26" name="form_id" />
												<input type="hidden" value="2" name="submit" />

											</ul>
										</form>
									</div>
								</div>
								</header> <!-- end article header -->

						    <section class="post-content clearfix hero wrap clearfix" itemprop="articleBody">

									<nav class="col-md--fourcol sidebar">
										<h4>Archives</h4>
									</nav>
									<div class="col-md--eightcol">
							    	<?php the_content(); ?>
									</div>
							</section> <!-- end article section -->

						    <footer class="article-footer wrap clearfix">

							    <?php the_tags('<p class="tags"><span class="tags-title">Tags:</span> ', ', ', '</p>'); ?>

						    </footer> <!-- end article footer -->

						    <?php //comments_template(); ?>

					    </article> <!-- end article -->
					    <?php endwhile; ?>

    				</div> <!-- end #main -->

		</div> <!-- end #content -->


	<?php endif; ?>

<?php get_footer(); ?>
