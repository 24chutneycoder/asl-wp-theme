<?php 
include_once('bundles/acf-field-date-time-picker/acf-date_time_picker.php');

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_links',
		'title' => 'Links',
		'fields' => array (
			array (
				'key' => 'field_52dd730b7101c',
				'label' => 'Is the content here on this website?',
				'name' => 'link_options',
				'type' => 'radio',
				'required' => 1,
				'choices' => array (
					'yes' => 'Yep',
					'no' => 'It\'s an external link',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => 'yes',
				'layout' => 'vertical',
			),
			array (
				'key' => 'field_52dd71531ec70',
				'label' => 'Content',
				'name' => 'url',
				'type' => 'page_link',
				'instructions' => 'Where do you want to link?',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_52dd730b7101c',
							'operator' => '==',
							'value' => 'yes',
						),
					),
					'allorany' => 'all',
				),
				'post_type' => array (
					0 => 'all',
				),
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_52dd73ad35d2f',
				'label' => 'URL',
				'name' => 'url',
				'type' => 'text',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_52dd730b7101c',
							'operator' => '==',
							'value' => 'no',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => 'http://',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52dd77b8ab635',
				'label' => 'What do you want the button to say?',
				'name' => 'button_text',
				'type' => 'text',
				'default_value' => 'Check it Out',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
					'order_no' => 0,
					'group_no' => 0,
				),
				array (
					'param' => 'post_format',
					'operator' => '==',
					'value' => 'link',
					'order_no' => 1,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

?>